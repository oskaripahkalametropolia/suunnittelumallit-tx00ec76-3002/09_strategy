import java.util.Iterator;
import java.util.List;

public class ListStringifyIterator implements ListStringifyStrategy {
    public String listToString(List list){
        StringBuilder output = new StringBuilder();
        Iterator listIterator = list.iterator();
        while (listIterator.hasNext()){
            output.append("%s,%n".formatted(listIterator.next()));
        };
        return output.toString();
    }
}
