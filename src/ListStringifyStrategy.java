import java.util.List;

public interface ListStringifyStrategy {
    String listToString(List list);
}
