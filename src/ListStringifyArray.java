import java.util.List;

public class ListStringifyArray implements ListStringifyStrategy {
    public String listToString(List list){
        StringBuilder output = new StringBuilder();
        int i = 0;
        for (Object element: list.toArray()) {
            if (i != 0 && i%2 == 0) output.append("%n".formatted());
            output.append("%s,".formatted(element));
            ++i;
        }
        output.append("%n".formatted());
        return output.toString();
    }
}
