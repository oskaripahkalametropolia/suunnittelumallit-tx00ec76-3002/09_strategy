import java.util.List;

public class ListStringifyGet implements ListStringifyStrategy {
   public String listToString(List list){
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            if (i != 0 && i%3 == 0) output.append("%n".formatted());
            output.append("%s,".formatted(list.get(i)));
        }
        output.append("%n".formatted());
        return output.toString();
    }
}
