import java.util.List;

public class ListStringify {
    private ListStringifyStrategy strategy;

    public String listToString(List list) {
        if (strategy == null){
            throw new RuntimeException("Strategy has not been set.");
        }
        return strategy.listToString(list);
    }

    public void setStrategy(ListStringifyStrategy strategy) {
        this.strategy = strategy;
    }
}
