import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ArrayList<Integer> list = (ArrayList<Integer>)List.of(48, 68, 53, 28, 54, 40, 39, 22, 24, 37);
        ListStringify stringify = new ListStringify();

        stringify.setStrategy(new ListStringifyIterator());
        System.out.println(stringify.listToString(list));

        stringify.setStrategy(new ListStringifyArray());
        System.out.println(stringify.listToString(list));

        stringify.setStrategy(new ListStringifyGet());
        System.out.println(stringify.listToString(list));
    }
}
